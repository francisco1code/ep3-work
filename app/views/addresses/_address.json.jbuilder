json.extract! address, :id, :rua, :cep, :telefone, :referencia, :email, :requisito, :job_id, :created_at, :updated_at
json.url address_url(address, format: :json)
