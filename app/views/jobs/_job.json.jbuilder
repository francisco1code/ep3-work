json.extract! job, :id, :resumo, :cargo, :salario, :vagas, :created_at, :updated_at
json.url job_url(job, format: :json)
