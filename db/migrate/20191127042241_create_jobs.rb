class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.string :resumo
      t.string :cargo
      t.string :salario
      t.integer :vagas

      t.timestamps
    end
  end
end
