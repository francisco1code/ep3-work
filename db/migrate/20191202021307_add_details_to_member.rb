class AddDetailsToMember < ActiveRecord::Migration[5.2]
  def change
    add_column :members, :nome, :string
    add_column :members, :telefone, :string
    add_column :members, :cpf, :string
  end
end
