class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :rua
      t.string :cep
      t.string :telefone
      t.string :referencia
      t.string :email
      t.string :requisito
      t.references :job, foreign_key: true

      t.timestamps
    end
  end
end
