# README



# Sobre o work now

## Resumo do Funcionamento

![fluxo](https://gitlab.com/francisco170142329/ep3-work/raw/master/Imagem/ttt.png)

## Oque é 
* A work now é uma plataforma baseada no,Sistema Nacional de Emprego (SINE). criado em 1975 como resultado da ratificação por parte do governo brasileiro, da convenção numero 88 da organização internacional do trabalho(OIT) 
## Como funciona 

* Assim como nos Sine o contratante apenas disponibiliza os dados da empresa como endereço, telefone e email, ficando por conta do trabalhador contato com a empresa para uma vaga de entrevista.

* O sistema é composto por dois tipos de usuários: o contratante e o trabalhador, Podendo existir vários usuários contratantes. Além disso, o usuário trabalhador pode se candidatar a varias vaga do emprego oferecido pelo sistema, mas é necessário se identificar no sistema para concorrer as vagas oferecidas pelos contratantes.
# Funcionalidade

## Cadastramento do contratante
  * Dado pessoas
      * Nome
      * Cpf
      * Email
## Cadastramento da vaga
  * Dados da vaga
      * resumo
      * salario
      * vaga
  * Dados da empresa
      * nome
      * endereço
      * telefone
      * email 
##  Reconhecimento do trabalhador
  * Dados pessoais do trabalhador
      * nome
      * cpf
      * email

# Pendencias
  ## Todo o projeto foi desenvolvido na linguagem ruby com o framework rails

* Ruby 2.5.5p157 (2019-03-15 revision 67260)

    ```sudo apt install ruby-full```
    
* Rails 5.2.2

    ```gem install rails```
    
* Bundler version 1.17.3


Se nenhum problema ocorrer, o Ruby on Rails estará instalado.      
 ## Preparar o habiente

* Baixar as dependencia
    
    ```bundle install```
          
* Banco de dados

    ```rake db:migrate```
          
* Subir o servidor na sua maquina

     ```rails s```   

* Acessar o site

    ```  http://localhost:3000/```   

# Diagramas

## Diagrama de uso

![su](https://gitlab.com/francisco170142329/ep3-work/raw/master/Imagem/du.png)


## Diagrama de classe

![mu](https://gitlab.com/francisco170142329/ep3-work/raw/master/Imagem/dm.png)
