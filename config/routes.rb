Rails.application.routes.draw do
  resources :addresses
  devise_for :members
  get 'home/index'

  root 'home#index'

  resources :jobs
  resources :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
